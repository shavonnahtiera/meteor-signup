if (Meteor.isClient) {

  Meteor.startup(function () {
    Session.set('resetPassword', false);
  });

  Template.signupForm.events({
    'submit #signup-form': function(e,t){
      e.preventDefault();

      Accounts.createUser({
        username: t.find('#signup-username').value,
        password: t.find('#signup-password').value,
        email: t.find('#signup-email').value,
        profile:{
          fullname:t.find('#signup-name').value
        }
      }, function (error) {
        if (error) {
          alert("Account is not created");
        }
      });
    }
  });

  Template.logoutForm.events({
    'submit #logout-form': function (e,t) {
      e.preventDefault();

      Meteor.logout(function(error){
        if(error){
          alert("Unable to logout from site.");
        }

      });
    }
  });

  Template.loginForm.events({
    'submit #login-form': function (e,t) {
      e.preventDefault();

      var unam=t.find('#login-username').value;
      var password=t.find('#login-password').value;

      // Accounts package
      Meteor.loginWithPassword(unam,password, function(error){
        if(error){
          alert("Oops. Something's not quite right. Try again.");
        }
      });
    }
  });

  Template.recovery.helpers({
    'resetPassword': function(t){
      if (Accounts._resetPasswordToken) {
        Session.set('resetToken', Accounts._resetPasswordToken);
        Session.set('resetPassword', true);
      }
      return Session.get('resetPassword');
    }
  });

  Template.recovery.events({
    'submit #recovery-form': function (e,t) {
      e.preventDefault();

      var email = t.find('#recovery-email').value;

      Accounts.forgotPassword({
        email: email
      }, function (error) {
        if (error) {
          alert("Unable to send Reset link")
        }
      });

    },
    'submit #new-password': function(e,t){
      e.preventDefault();

      var newPass=t.find('#new-password-password').value;

      Accounts.resetPassword(Session.get('resetToken'), newPass, function (error) {
        if (error) {
          alert("Password not changed");
        }
        else {
          alert("Password successfully updated")
        }
      });
    }
  });

}

if (Meteor.isServer) {
  Meteor.startup(function (){
    //forgotten Email setup (mailgun)
    process.env.MAIL_URL = 'smtp://username::password@host:port';
    Accounts.emailTemplates.from = "Verification Link";
  });
 
}
